# Welcome to Angelos Kafounis GitLab

Project 1:
---------------------------------------------------------------------------------------------------------

Rummikub - Java: To open the application there must be a Java Version installed at your computer.
                 There is a jar file at: /Abgaben/final_jar           
                 
Project 2:
---------------------------------------------------------------------------------------------------------

Bomberman - Javascript: To run the game there must be a valid NodeJS version install at your computer.
                        Then type at your terminal: "node server.js"

#Install
```bash
npm install
```

#Run
To build your bundle, run:
```bash
npm run build
```
or
```bash
webpack
```
This will create the `./dist` folder

To start the server:
```bash
node server
```
for development use (auto refresh):
```bash
nodemon server
```


Now open your browser and visit http://localhost:9000/.

Project 3:
---------------------------------------------------------------------------------------------------------
Readly - C#
Recently sarted. The Software ist currently under development!
**ATTENTION!** In order to test the software you have to use the **TOBII EYE TRACKER 4C.****