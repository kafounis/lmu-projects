﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Input.Preview;


// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace App1
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private GazeInputSourcePreview gazeInputSource;
        private GazeDeviceWatcherPreview gazeDeviceWatcher;

        private Word[] words;
        private string text = "Hello everybody, I am Angelos Kafounis.";




        public MainPage()
        {
            this.InitializeComponent();
            System.Diagnostics.Debug.WriteLine("Hello Hackers!");
            CreateWordsFromText(this.text);
            button.Text = "Good Morning There!";

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            StartGazeDeviceWatcher();
        }

        private async void TryEnableGazeTrackingAsync(GazeDevicePreview gazeDevice)
        {
            if (IsSupportedDevice(gazeDevice))
            {
                gazeInputSource = GazeInputSourcePreview.GetForCurrentView();
                gazeInputSource.GazeEntered += GazeEntered;
                gazeInputSource.GazeMoved += GazeMoved;
                gazeInputSource.GazeExited += GazeExited;
                
            } else
            {
                await gazeDevice.RequestCalibrationAsync();
            }


        }

        private void StartGazeDeviceWatcher()
        {
            if (gazeDeviceWatcher == null)
            {
                gazeDeviceWatcher = GazeInputSourcePreview.CreateWatcher();
                gazeDeviceWatcher.Added += this.DeviceAdded;
                //gazeDeviceWatcher.Updated += this.DeviceUpdated;
                gazeDeviceWatcher.Start();
            }

        }

        private void DeviceAdded(GazeDeviceWatcherPreview source, GazeDeviceWatcherAddedPreviewEventArgs args)
        {
            TryEnableGazeTrackingAsync(args.Device);
        }

        private void GazeEntered(GazeInputSourcePreview sender, GazeEnteredPreviewEventArgs args)
        {
            //Genutzt wenn entered.
            args.Handled = true;
        }

        private void GazeMoved(GazeInputSourcePreview sender, GazeMovedPreviewEventArgs args)
        {
            if (args.CurrentPoint.EyeGazePosition != null)
            {
                double gazePointX = args.CurrentPoint.EyeGazePosition.Value.X;
                double gazePointY = args.CurrentPoint.EyeGazePosition.Value.Y;
                //blubb.Text = "Point X: " + gazePointX + "; Point Y: "+ gazePointY;
                //System.Diagnostics.Debug.WriteLine("Point X: " + gazePointX + "; Point Y: " + gazePointY);
               //
            }


            //TODO: Collision Detection
            CheckForCollision(args.CurrentPoint.EyeGazePosition.Value.X, args.CurrentPoint.EyeGazePosition.Value.Y);

            args.Handled = true;

        }

        private void CheckForCollision(double x, double y)
        {
            for (int i = 0; i < this.words.Length; i++)
            {
                if (this.words[i].Check(x,y))
                {
                    this.words[i].increaseHaufigkeit(1);
                    blubb.Text = "Word: " + this.words[i].getWord();
                }
            }
        }

        private void GazeExited(GazeInputSourcePreview sender, GazeExitedPreviewEventArgs args)
        {
            args.Handled = true;
        }

        private bool IsSupportedDevice(GazeDevicePreview gazeDevice)
        {
            return (gazeDevice.CanTrackEyes && gazeDevice.ConfigurationState == GazeDeviceConfigurationStatePreview.Ready);
        }

        private void CreateWordsFromText(String text)
        {
            string[] tokens = text.Split(new string[] { " ", ";" }, StringSplitOptions.RemoveEmptyEntries);
            this.words = new Word[tokens.Length];
            int space = 10;
            for (int i = 0; i < tokens.Length; i++)
            {
                this.words[i] = new Word(tokens[i], i + space, 10);
                space += 10;
            }
        }

        

    }
    internal class Word
    {
        private string word;
        private float x;
        private float y;
        private int haufigkeit;
        private int color;

        public Word(string word, float x, float y)
        {
            this.word = word;
            this.x = x;
            this.y = y;
            this.haufigkeit = 0;
            this.color = 1;
        }

        public int getColor()
        {
            return this.color;
        }

        public void increaseHaufigkeit(int sec)
        {
            this.haufigkeit += sec;
            System.Diagnostics.Debug.WriteLine("You just looked at the word: " + this.word + ", haufigkeit: " + this.haufigkeit);
        }

        public bool Check(double x, double y)
        {
            if ( x > this.x + 10 | y > this.y + 10) {
                return false;
            } else if ( x < this.x - 10 | y < this.y - 10)
            {
                return false;
            }
            return true;
        }

        internal string getWord()
        {
            return this.word;
        }
    }
}